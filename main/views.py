from django.shortcuts import render

from django.views.generic.base import ContextMixin
from django.views.generic import TemplateView


class HeaderMixin(ContextMixin):
  def get_header(self):
    # form header as we need
    header = "Menu"
    return header

  def get_context_data(self, **kwargs):
    ctx = super(HeaderMixin, self).get_context_data(**kwargs)
    ctx['header'] = self.get_header()
    return ctx


class FooterMixin(ContextMixin):
  def get_footer(self):
    # form footer as we need
    footer = "Site footer"
    return footer

  def get_context_data(self, **kwargs):
    ctx = super(FooterMixin, self).get_context_data(**kwargs)
    ctx['footer'] = self.get_footer()
    return ctx


class PageView(HeaderMixin, FooterMixin, TemplateView):
    template_name = "main/page.html"

    def get_context_data(self, **kwargs):
        context = super(PageView, self).get_context_data(**kwargs)
        context['content'] = 'some text content'
        return context


class GallaryView(HeaderMixin, FooterMixin, TemplateView):
    template_name = "main/gallary.html"

    def get_context_data(self, **kwargs):
        context = super(GallaryView, self).get_context_data(**kwargs)
        context['photos'] = 'photo gallary'
        return context

class GameListView(HeaderMixin, FooterMixin, TemplateView):
    template_name = "main/game_list.html"

    def get_context_data(self, **kwargs):
        context = super(GameListView, self).get_context_data(**kwargs)
        context['game_list'] = ["Battle Field 2", "World of Tanks"]
        return context

