from django.conf.urls import url
from main.views import PageView, GallaryView, GameListView

app_name = 'main'

urlpatterns = [
    url(r'^$', PageView.as_view(), name='page'),
    url(r'^gallary/$', GallaryView.as_view(), name='gallary'),
    url(r'^games/$', GameListView.as_view(), name='games'),
]